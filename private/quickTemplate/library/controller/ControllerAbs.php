<?php
declare(strict_types=1);

namespace quickTemplate\library\controller;

use quickTemplate\library\controller\ControllerItf;
use quickTemplate\page\badRequest\library\controller\BadRequestController;

abstract class ControllerAbs implements ControllerItf
{

	protected bool $isInputValid;

	public function action():void
	{
		$this->isInputValid = true;

		$this->validateInput();

		if (!$this->isInputValid)
		{
			$badRequestController = new BadRequestController();
			$badRequestController->action();
			return;
		}

		$this->run();

		$this->respond();
	}

		protected function validateInput():void
		{
		}

		protected function run():void
		{
		}

		protected function respond():void
		{
		}

}
