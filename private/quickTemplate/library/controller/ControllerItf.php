<?php
declare(strict_types=1);

namespace quickTemplate\library\controller;

interface ControllerItf
{

	public function action():void;

}