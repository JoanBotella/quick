<?php
declare(strict_types=1);

namespace quickTemplate\page\badRequest\library\controller;

use quickTemplate\library\controller\ControllerAbs;
use quickTemplate\page\badRequest\service\widget\mainContent\BadRequestMainContentWidget;
use quickTemplate\service\widget\layout\LayoutWidget;

final class BadRequestController extends ControllerAbs
{

	protected function respond():void
	{
		http_response_code(400);

		$context = [
			'languageCode' => 'en',
			'pageCode' => 'bad_request',
			'pageTitle' => 'Bad Request',
		];

		$context['mainContent'] = BadRequestMainContentWidget::render($context);

		echo LayoutWidget::render($context);
	}

}