<?php
declare(strict_types=1);

namespace quickTemplate\page\internalServerError\service\widget\mainContent;

use quickTemplate\service\templater\Templater;

final class InternalServerErrorMainContentWidget
{

	public static function render(array $context):string
	{
		return Templater::render(
			__DIR__.'/template.php',
			$context
		);
	}

}