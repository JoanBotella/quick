<?php
declare(strict_types=1);

namespace quickTemplate\page\home\service\urlBuilder;

final class HomeUrlBuilder
{

	public static function build():string
	{
		return 'home';
	}

}