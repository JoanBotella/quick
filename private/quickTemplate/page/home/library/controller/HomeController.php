<?php
declare(strict_types=1);

namespace quickTemplate\page\home\library\controller;

use quickTemplate\library\controller\ControllerAbs;
use quickTemplate\page\home\service\widget\mainContent\HomeMainContentWidget;
use quickTemplate\service\widget\layout\LayoutWidget;

final class HomeController extends ControllerAbs
{

	protected function respond():void
	{
		$context = [
			'languageCode' => 'en',
			'pageCode' => 'home',
			'pageTitle' => 'Home',
		];

		$context['mainContent'] = HomeMainContentWidget::render($context);

		echo LayoutWidget::render($context);
	}

}