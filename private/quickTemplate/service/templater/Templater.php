<?php
declare(strict_types=1);

namespace quickTemplate\service\templater;

final class Templater
{

	public static function render(string $filePath, array $context = []):string
	{
		ob_start();
		require $filePath;
		return ob_get_clean();
	}

}