<?php
declare(strict_types=1);

namespace quickTemplate\service\widget\layout;

use quickTemplate\service\templater\Templater;

final class LayoutWidget
{

	public static function render(array $context = []):string
	{
		return Templater::render(
			__DIR__.'/template.php',
			$context
		);
	}

}