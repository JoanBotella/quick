<?php
declare(strict_types=1);

namespace quickTemplate\service\router;

use Exception;
use quickTemplate\page\internalServerError\library\controller\InternalServerErrorController;
use quickTemplate\page\notFound\library\controller\NotFoundController;
use quickTemplate\library\controller\ControllerItf;
use quickTemplate\service\requestPathContainer\RequestPathContainer;

final class Router
{

	public static function route():void
	{
		try
		{
			$controller = self::buildController();
			$controller->action();
		}
		catch (Exception $e)
		{
			$controller = new InternalServerErrorController();
			$controller->action();
		}
	}

		private function buildController():ControllerItf
		{
			$result = preg_match(
				'/^([^?\/]*)/',
				RequestPathContainer::get(),
				$matches
			);

			if ($result !== 1)
			{
				throw new Exception('Page controller could not be guessed');
			}

			$pageSlug = 
				$matches[1] == ''
					? 'home'
					: $matches[1]
			;

			$classNamespace = '\\quickTemplate\\page\\'.$pageSlug.'\\library\\controller\\'.ucfirst($pageSlug).'Controller';

			if (class_exists($classNamespace))
			{
				return new $classNamespace();
			}

			return new NotFoundController();
		}

}