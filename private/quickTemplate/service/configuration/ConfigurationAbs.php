<?php
declare(strict_types=1);

namespace quickTemplate\service\configuration;

abstract class ConfigurationAbs
{

	abstract public static function getBaseRequestUri():string;

}