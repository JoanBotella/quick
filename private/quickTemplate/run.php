<?php
declare(strict_types=1);

require __DIR__.'/setup.php';

use quickTemplate\service\router\Router;

Router::route();