'use strict';

const
	gulp = require('gulp'),
	concat = require('gulp-concat'),
	rename = require('gulp-rename'),
	minifyJs = require('gulp-terser'),
	minifyCss = require('gulp-csso'),
	del = require('del')
;

const
	privateBaseDir = '../quickTemplate/',
	publicBaseDir = '../../public/',
	pages = [
		'badRequest',
		'home',
		'internalServerError',
		'notFound',
	]
;

const deleteDir = function (dir)
{
	del.sync(
		[
			dir + '/**',
		],
		{
			force: true
		}
	);
}

function css(done)
{
	const publicCssDir = publicBaseDir + 'asset/css';

	let sourceCssFiles = [
		privateBaseDir + 'asset/css/head.css',
		privateBaseDir + 'asset/css/general.css',
	];

	for (let i = 0; i < pages.length; i++)
	{
		sourceCssFiles.push(
			privateBaseDir + 'page/' + pages[i] + '/asset/css/style.css'
		);
	}

	deleteDir(publicCssDir);

	gulp
		.src(sourceCssFiles)
		.pipe(concat('style.css'))
		.pipe(gulp.dest(publicCssDir))
		.pipe(rename('style.min.css'))
		.pipe(minifyCss())
		.pipe(gulp.dest(publicCssDir))
	;

	done();
}

function img(done)
{
	const publicImgDir = publicBaseDir + 'asset/img';

	let sourceImgDirs = [
		privateBaseDir + 'asset/img/**',
	];

	for (let i = 0; i < pages.length; i++)
	{
		sourceImgDirs.push(
			privateBaseDir + 'page/' + pages[i] + '/asset/img/**'
		);
	}

	deleteDir(publicImgDir);

	gulp
		.src(sourceImgDirs)
		.pipe(gulp.dest(publicImgDir))
	;

	done();
}

function js(done)
{
	const publicJsDir = publicBaseDir + 'asset/js';

	let sourceJsFiles = [
		privateBaseDir + 'asset/js/head.js',
	];

	for (let i = 0; i < pages.length; i++)
	{
		sourceJsFiles.push(
			privateBaseDir + 'page/' + pages[i] + '/asset/js/script.js'
		);
	}

	sourceJsFiles.push(
		privateBaseDir + 'asset/js/route.js'
	);

	sourceJsFiles.push(
		privateBaseDir + 'asset/js/tail.js'
	);

	deleteDir(publicJsDir);

	gulp
		.src(sourceJsFiles)
		.pipe(concat('script.js'))
		.pipe(gulp.dest(publicJsDir))
		.pipe(rename('script.min.js'))
		.pipe(minifyJs())
		.pipe(gulp.dest(publicJsDir))
	;

	done();
}

const watchCss = function ()
{
	let privateCssDirs = [
		privateBaseDir + 'asset/css',
	];

	for (let i = 0; i < pages.length; i++)
	{
		privateCssDirs.push(
			privateBaseDir + 'page/' + pages[i] + '/asset/css'
		);
	}

	gulp.watch(
		privateCssDirs,
		{
		},
		gulp.parallel(
			css
		)
	);
}

const watchImg = function ()
{
	let privateImgDirs = [
		privateBaseDir + 'asset/img',
	];

	for (let i = 0; i < pages.length; i++)
	{
		privateImgDirs.push(
			privateBaseDir + 'page/' + pages[i] + '/asset/img'
		);
	}

	gulp.watch(
		privateImgDirs,
		{
		},
		gulp.parallel(
			img
		)
	);
}

const watchJs = function ()
{
	let privateJsDirs = [
		privateBaseDir + 'asset/js',
	];

	for (let i = 0; i < pages.length; i++)
	{
		privateJsDirs.push(
			privateBaseDir + 'page/' + pages[i] + '/asset/js'
		);
	}

	gulp.watch(
		privateJsDirs,
		{
		},
		gulp.parallel(
			js
		)
	);
}

watchCss();
watchImg();
watchJs();

exports.default = gulp.parallel(
	css,
	img,
	js
);
